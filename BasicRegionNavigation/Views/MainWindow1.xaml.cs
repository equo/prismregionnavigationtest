﻿using Prism.Regions;
using System.Windows;

namespace BasicRegionNavigation.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow1 : Window
    {
        private IRegionManager _RegionManager;
        private IRegionManager _ScopedRegion;

        public MainWindow1(IRegionManager regionManager)
        {
            InitializeComponent();

            _RegionManager = regionManager;

            _ScopedRegion = _RegionManager.CreateRegionManager();
            RegionManager.SetRegionManager(this, _ScopedRegion);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _ScopedRegion.RequestNavigate("NewRegion", "ViewA");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            _ScopedRegion.RequestNavigate("NewRegion", "ViewB");
        }
    }
}
